#include <iostream>
#include <list>

#include <Amount.h>
#include <Complex.h>
#include <exception>
#include<algorithm>

/*template<typename T>
class Vector
{
public:
	Vector(uint16_t defaultVectorSize = 10) :m_size(0), m_array(new T[defaultVectorSize]), m_capacity(defaultVectorSize) {}
	void push_back(T elem)
	{
		if (m_size == m_capacity)
			resize(2 * m_size);
		m_array[m_size] = elem;
		++m_size;
	}

	T& operator[](const uint16_t index)
	{
		if (index < 0)
			std::cout << "bad programmer";
		else if (index >= m_size)
			std::cout << "oops, better luck next time";
		return m_array[index];
	}

	void print()
	{
		for (uint16_t i = 0; i < m_size; i++)
			std::cout << m_array[i] << " ";
	}

	void clear()
	{
		m_size = 0;
	}

	void pop_back()
	{
		--m_size;
	}

	T& at(const uint16_t index)
	{
		return m_array[index];
	}

private:
	void resize(const uint16_t size_)
	{
		if (size_ > m_capacity)
		{
			T* temp = new T[size_];
			memcpy(temp, m_array, m_size * sizeof(T));
			std::swap(temp, m_array);
			delete[] temp;
			m_capacity = size_;
		}
	}
	T* m_array;
	uint16_t m_size;
	uint16_t m_capacity;
};*/

template <typename T>
class Stack
{
private:
	int m_size;
	int m_capacity;
	T * m_array;
	int currentIndex;
	T top;

public:
	Stack(int defaultStackSize = 10) : m_capacity(defaultStackSize), m_size(defaultStackSize), m_array(new T[defaultStackSize]), currentIndex(-1) {}

	void push(const T& element)
	{
		top = element;
		if (m_size == m_capacity)
		{
			resize(2 * m_size);
		}
		if (currentIndex < m_size)
		{
			m_array[++currentIndex] = element;
		}

		sort();
	};

	T pop()
	{
		sort();
		try
		{
			if (currentIndex > -1)
			{
				return m_array[currentIndex--];
			}
		}
		catch (const std::exception& e)
		{
			std::cout << e.what() << std::endl;
		}
	};

	bool isEmpty()
	{
		if (currentIndex < 0)
		{
			return true;
		}
		return false;
	}

private:
	void resize(const int size_)
	{
		if (size_ > m_capacity)
		{
			T* temp = new T[size_];
			memcpy(temp, m_array, m_size * sizeof(T));
			std::swap(temp, m_array);
			delete[] temp;
			m_capacity = size_;
		}
	}

public:
	void sort()
	{
		if (currentIndex != -1)
			std::sort(m_array, m_array + currentIndex + 1);
	}

	void print()
	{
		for (int i = 0; i <= currentIndex; i++)
		{
			std::cout << m_array[i] << " ";
		}

		std::cout << std::endl;
	}

};

bool compareComplex(const Complex& a, const Complex& b)
{
	return sqrt(a.real*a.real + a.imaginary*a.imaginary) < sqrt(b.real*b.real + b.imaginary*b.imaginary);
}

template <>
void Stack<Complex>::sort()
{
	if (currentIndex != -1)
		std::sort(m_array, m_array + currentIndex + 1, compareComplex);
}


int main()
{
	Stack<Complex> complexStack(5);
	complexStack.push(Complex(5, 7));
	complexStack.push(Complex(10, 2));
	complexStack.push(Complex(7, 9));
	complexStack.push(Complex(2, 3));
	complexStack.sort();
	complexStack.print();

	Stack <int> stack(5);
	stack.push(5);
	stack.push(1);
	stack.push(2);
	stack.push(53);
	stack.push(13);
	stack.push(23);
	stack.print();

		/*
		Vector<int> vec(2);
		vec.push_back(1);
		vec.clear();
		vec.push_back(2);
		vec.push_back(2);
		vec.push_back(2);
		std::cout << vec.at(0);
		std::cout << " ";
		std::cout << vec.at(1);
		std::cout << " ";
		std::cout << vec.at(2);
		std::cout << " ";
		std::cout << vec[3];
		std::cout << " ";

		Vector<double> vec1(3);
		vec1.push_back(3.2);
		vec1.push_back(3.2);
		vec1.push_back(3.2);
		vec1.print();
		*/

		return 0;
}